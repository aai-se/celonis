/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.celonis;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.celonis.utils.CROperations;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "saveaarievents", label = "Save Control Room AARI Events", 
		node_label = "Save AARI Events {{filename}}", description = "Save Control Room AARI Events", comment = true ,  text_color = "#000000" , background_color =  "#c6cacc" , icon = "pkg.svg", 
		return_label = "More?", return_type = DataType.BOOLEAN, return_required = true)

public class SaveAARIEvents {
	
    @Sessions
    private Map<String, Object> sessions;
    
	@com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	private GlobalSessionContext globalSessionContext;
  
	public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	}

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public BooleanValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Celonis") @NotEmpty String sessionName,
						      @Idx(index = "2", type = AttributeType.FILE)  @Pkg(label = "CSV File Name" , default_value_type = DataType.FILE ) @NotEmpty String filename,
						      @Idx(index = "3", type = AttributeType.NUMBER)  @Pkg(label = "Offset" , description = "Increase in steps of 1000 if more events" ,default_value_type = DataType.NUMBER ) @NotEmpty Number offset,
						      @Idx(index = "4", type = AttributeType.BOOLEAN)  @Pkg(label = "Append" , default_value_type = DataType.NUMBER ,default_value = "false") @NotEmpty Boolean append,
						      @Idx(index = "5", type = AttributeType.LIST)  @Pkg(label = "Process Names" , default_value_type = DataType.STRING ,description ="If empty, all process events will be saved") List<Value> processnames
								
						      ) throws Exception	
   {

	    String token = this.globalSessionContext.getUserToken();
        String url = this.globalSessionContext.getCrUrl();

        CROperations crOp = new CROperations()
				.setCrurl(url)
				.setToken(token);
        Integer rows = 0;
        
        if (processnames == null) {
    		rows = crOp.getAARIEvents( filename,null, offset.intValue(),append);
        } else if (processnames.size() == 0) {
        	rows = crOp.getAARIEvents( filename,null, offset.intValue(),append);
        } else {	
        	List<String> processList = new ArrayList<String>();
        	for (Iterator iterator = processnames.iterator(); iterator.hasNext();) {
				StringValue processname = (StringValue) iterator.next();
				processList.add(processname.get());
				rows = crOp.getAARIEvents( filename,processList, offset.intValue(),append);
			}
        }

		Boolean more = (rows >= CROperations.maxEvents) ? true : false;
		return new BooleanValue(more);

    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
   }
}
