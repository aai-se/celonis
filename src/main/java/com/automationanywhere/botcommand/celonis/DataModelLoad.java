/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.celonis;

import com.automationanywhere.botcommand.celonis.utils.CelonisEventCollection;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "datamodelload", label = "Data Model Load", 
		node_label = "Data Model Load {{modelid}}", description = "Data Model Load", comment = true ,  text_color = "#000000" , background_color =  "#c6cacc" , icon = "pkg.svg", 
		return_label = "Status", return_type = DataType.STRING, return_required = false)

public class DataModelLoad {
	
    @Sessions
    private Map<String, Object> sessions;
  

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Celonis") @NotEmpty String sessionName,
						      @Idx(index = "2", type = TEXT)  @Pkg(label = "Data Model Id" , default_value_type = STRING ) @NotEmpty String modelid,
						      @Idx(index = "3", type = AttributeType.LIST)  @Pkg(label = "Tables" , default_value_type = STRING ) @NotEmpty List<Value> tables

						      ) throws Exception	
   {


		CelonisEventCollection celonisCollection  = (CelonisEventCollection) this.sessions.get(sessionName);  

		String[] tablesArray = new String[tables.size()];
		int i =0;
		for (Iterator iterator = tables.iterator(); iterator.hasNext();) {
			StringValue table = (StringValue) iterator.next();
			tablesArray[i++] = table.get();
		}
		
		String status = celonisCollection.dataModelLoad(modelid,tablesArray);

		return new StringValue(status);

    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
   }
}
