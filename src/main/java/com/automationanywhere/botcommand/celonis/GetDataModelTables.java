/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.celonis;

import com.automationanywhere.botcommand.celonis.utils.CelonisEventCollection;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;


import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "listmodeltables", label = "Data Model Tables", 
		node_label = "Data Model Tables {{datamodelid}}", description = "List of Data Model Tables", comment = true ,  text_color = "#000000" , background_color =  "#c6cacc" , icon = "pkg.svg", 
		return_label = "Table Ids", return_type = DataType.LIST, return_sub_type = DataType.STRING , return_required = true)

public class GetDataModelTables {
	
    @Sessions
    private Map<String, Object> sessions;
  

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public ListValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Celonis") @NotEmpty String sessionName,
							@Idx(index = "2", type = TEXT)  @Pkg(label = "Data Model Id" , default_value_type = STRING ) @NotEmpty String datamodelid
						      ) throws Exception	
   {


		CelonisEventCollection celonisCollection  = (CelonisEventCollection) this.sessions.get(sessionName);  

		String[] tables = celonisCollection.getDatasModelTables(datamodelid);
		
		List<Value> listofresults = new ArrayList<Value>();

		for (int i = 0; i < tables.length; i++) {
			listofresults.add(new StringValue(tables[i]));
		}

		ListValue<StringValue> finalresult = new ListValue<StringValue>();
		finalresult.set(listofresults);
		return finalresult;


    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
   }
}
