/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.celonis;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.celonis.utils.CROperations;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.annotations.rules.SelectModes;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "saveauditevents", label = "Save Control Room Audit Events", 
		node_label = "Save Audit Events {{filename}}", description = "Save Control Room Audit Events", comment = true ,  text_color = "#000000" , background_color =  "#c6cacc" , icon = "pkg.svg", 
		return_label = "More?", return_type = DataType.BOOLEAN, return_required = true)

public class SaveAuditEvents {
	
    @Sessions
    private Map<String, Object> sessions;
    
	@com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	private GlobalSessionContext globalSessionContext;
  
	public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	}

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public BooleanValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Celonis") @NotEmpty String sessionName,
						      @Idx(index = "2", type = AttributeType.FILE)  @Pkg(label = "CSV File Name" , default_value_type = DataType.FILE) @NotEmpty String filename,
						      @Idx(index = "3", type = AttributeType.NUMBER)  @Pkg(label = "Offset" , description = "Increase in steps of 1000 if more events" ,default_value_type = DataType.NUMBER ) @NotEmpty Number offset,
						      @Idx(index = "4", type = AttributeType.BOOLEAN)  @Pkg(label = "Append" , default_value_type = DataType.NUMBER ,default_value = "false") @NotEmpty Boolean append,
							  @Idx(index = "5", type = AttributeType.SELECT, options = {
									  	@Idx.Option(index = "5.1", pkg = @Pkg(label = "Table", value = "TABLE")), 
									  	@Idx.Option(index = "5.2", pkg = @Pkg(label = "JSON", value = "JSON"))})
			           					@Pkg(label = "Filter", description = "", default_value = "TABLE", default_value_type = STRING)
					   						@SelectModes  @NotEmpty String filtertype,
											@Idx(index = "5.1.1", type = SELECT, options = {
													  @Idx.Option(index = "5.1.1.1", pkg = @Pkg(label = "AND", value = "and")),
													  @Idx.Option(index = "5.1.1.2", pkg = @Pkg(label = "OR", value = "or"))
											      }) @Pkg(label = "Operand", default_value = "and", default_value_type = STRING) @NotEmpty String operand,
					   						@Idx(index = "5.1.2", type = AttributeType.TABLE)  @Pkg(label = "Operands" ,description = "Mandatory Columns: 'field','operand','value'", default_value_type = DataType.TABLE )  Table operands,
					   						@Idx(index = "5.2.1", type = AttributeType.TEXTAREA) @Pkg(label = "Filter JSON",  default_value_type = STRING)  String filter
								
						      ) throws Exception	
   {

	    String token = this.globalSessionContext.getUserToken();
        String url = this.globalSessionContext.getCrUrl();

        CROperations crOp = new CROperations()
				.setCrurl(url)
				.setToken(token);
    	String[][] operandsTable = new String[0][3];
    	filter = (filter == null) ? "" : filter;
    	if (filtertype.equals("TABLE")) {
    		if (operands != null) {
    			if (operands.getRows().size() > 0 && operands.getSchema().size() == 3) {
	    			List<Row> operandList = operands.getRows();
	    			operandsTable = new String[operandList.size()][3];
	    			int i=0;
		    		for (Iterator iterator = operandList.iterator(); iterator.hasNext();) {
		    			Row row = (Row) iterator.next();
		    			List<Value> columnValues = row.getValues();
		    			operandsTable[i][0] = columnValues.get(0).toString();
		    			operandsTable[i][1] = columnValues.get(1).toString();
		    			operandsTable[i][2] = columnValues.get(2).toString();
		    			i++;
		    		}
    			}
    		}
        } 
    	Integer rows = crOp.getAuditEvents(operand, operandsTable, filter, filename,offset.intValue(),append);


		Boolean more = (rows >= CROperations.maxEvents) ? true : false;
		return new BooleanValue(more);

    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
   }
}
