package com.automationanywhere.botcommand.celonis.utils;

import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CelonisEventCollection {
	
	private String celonis_cloud_url;
	private String bearer_token;
	private String dataPoolId;
	private final String base_url = "/integration/api/v1/";
	
	
	public CelonisEventCollection(String url,String token, String dataPool) {
		
		this.celonis_cloud_url = url;
		this.bearer_token = token;
		this.dataPoolId = dataPool;
		
	}
	
	public String newAuditDataPushJob(String type, String targetName, Boolean createTarget) throws  Exception {
		OkHttpClient client = new OkHttpClient().newBuilder().build();
		MediaType mediaType = MediaType.parse("application/json");
		JSONObject bodyJson = new JSONObject();
		bodyJson.put("type",type);
		bodyJson.put("fileType","CSV");
		bodyJson.put("dataPoolId",this.dataPoolId);
		bodyJson.put("targetName",targetName);
		String strategy = (createTarget) ? "UPSERT_WITH_NULLIFICATION" : "UPSERT_WITH_UNCHANGED_METADATA";
		bodyJson.put("upsertStrategy",strategy);
		
		JSONObject parsingOptions = new JSONObject();
		parsingOptions.put("decimalSeparator", ";");
		parsingOptions.put("separatorSequence", ",");
		parsingOptions.put("lineEnding","\n");
		parsingOptions.put("dateFormat","yyyy-MM-dd HH:mm:ss");
		
		bodyJson.put("csvParsingOptions", parsingOptions);
		
		JSONObject tableSchema = new JSONObject();
		tableSchema.put("tableName", targetName);
		
		JSONArray tableColumns = new JSONArray();
		JSONObject columnObject = new JSONObject("{\"columnName\":\"Status\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Time\",\"columnType\":\"DATETIME\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Item name\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Event type\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Event started by\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Source device\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Source\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Request ID\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		
		tableSchema.put("columns",tableColumns);
		
		bodyJson.put("tableSchema",tableSchema);
		
		
		RequestBody body = RequestBody.create(mediaType, bodyJson.toString());
		Request request = new Request.Builder()
			  .url(this.celonis_cloud_url+this.base_url+"data-push/"+this.dataPoolId+"/jobs/")
			  .method("POST", body)
			  .addHeader("Content-Type", "application/json")
			  .addHeader("Authorization", "Bearer "+this.bearer_token)
			  .build();
		Response response = client.newCall(request).execute();
		
		if (response.isSuccessful()) {
			JSONObject responseObj = new JSONObject(response.body().string());
			return responseObj.getString("id");
			
		}
		else {
			String responsebody = response.body().string();
			throw new Exception("ERROR "+response.code()+":"+responsebody);

		}
	}
	
	
	public String newAARIDataPushJob(String type, String targetName, Boolean createTarget) throws  Exception {
		OkHttpClient client = new OkHttpClient().newBuilder().build();
		MediaType mediaType = MediaType.parse("application/json");
		JSONObject bodyJson = new JSONObject();
		bodyJson.put("type",type);
		bodyJson.put("fileType","CSV");
		bodyJson.put("dataPoolId",this.dataPoolId);
		bodyJson.put("targetName",targetName);
		String strategy = (createTarget) ? "UPSERT_WITH_NULLIFICATION" : "UPSERT_WITH_UNCHANGED_METADATA";
		bodyJson.put("upsertStrategy",strategy);
		
		JSONObject parsingOptions = new JSONObject();
		parsingOptions.put("decimalSeparator", ";");
		parsingOptions.put("separatorSequence", ",");
		parsingOptions.put("lineEnding","\n");
		parsingOptions.put("dateFormat","yyyy-MM-dd HH:mm:ss");
		
		bodyJson.put("csvParsingOptions", parsingOptions);
		
		JSONObject tableSchema = new JSONObject();
		tableSchema.put("tableName", targetName);
		
		JSONArray tableColumns = new JSONArray();
		JSONObject columnObject = new JSONObject("{\"columnName\":\"Request ID\",\"columnType\":\"INTEGER\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Request Title\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Time\",\"columnType\":\"DATETIME\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Process Name\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Task ID\",\"columnType\":\"INTEGER\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Task Title\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Task Type\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"User\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		columnObject = new JSONObject("{\"columnName\":\"Status\",\"columnType\":\"STRING\",\"fieldLength\":80}");
		tableColumns.put(columnObject);
		
		tableSchema.put("columns",tableColumns);
		
		bodyJson.put("tableSchema",tableSchema);
		
		
		RequestBody body = RequestBody.create(mediaType, bodyJson.toString());
		Request request = new Request.Builder()
			  .url(this.celonis_cloud_url+this.base_url+"data-push/"+this.dataPoolId+"/jobs/")
			  .method("POST", body)
			  .addHeader("Content-Type", "application/json")
			  .addHeader("Authorization", "Bearer "+this.bearer_token)
			  .build();
		Response response = client.newCall(request).execute();
		
		if (response.isSuccessful()) {
			JSONObject responseObj = new JSONObject(response.body().string());
			return responseObj.getString("id");
			
		}
		else {
			String responsebody = response.body().string();
			throw new Exception("ERROR "+response.code()+":"+responsebody);

		}
	}
	
	
	public String pushFile(String fileName, String jobId) throws  Exception {
		OkHttpClient client = new OkHttpClient().newBuilder()
					.writeTimeout(60, TimeUnit.SECONDS)
					.build();
		MediaType mediaType = MediaType.parse("multipart/form-data");
		RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
				  .addFormDataPart("file",fileName,
				    RequestBody.create(MediaType.parse("application/octet-stream"),
				    new File(fileName)))
				  .build();
		Request request = new Request.Builder()
				  .url(this.celonis_cloud_url+this.base_url+"data-push/"+this.dataPoolId+"/jobs/"+jobId+"/chunks/upserted")
				  .method("POST", body)
				  .addHeader("Content-Type", "multipart/form-data")
				  .addHeader("Authorization", "Bearer "+this.bearer_token)
				  .build();
		Response response = client.newCall(request).execute();
		if (response.isSuccessful()) {
			return "SUCCESS";
			
		}
		else {
			String responsebody = response.body().string();
			throw new Exception("ERROR "+response.code()+":"+responsebody);

		}
	}
	
	
	public String executeDataJob(String jobId) throws  Exception {
		
		OkHttpClient client = new OkHttpClient().newBuilder()
				  .build();
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, "");
		Request request = new Request.Builder()
				  .url(this.celonis_cloud_url+this.base_url+"data-push/"+this.dataPoolId+"/jobs/"+jobId)
				  .method("POST", body)
				  .addHeader("Content-Type", "application/json")
				  .addHeader("Authorization", "Bearer "+this.bearer_token)
				  .build();
		
		Response response = client.newCall(request).execute();
		if (response.isSuccessful()) {
			return "SUCCESS";
			
		}
		else {
			String responsebody = response.body().string();
			throw new Exception("ERROR "+response.code()+":"+responsebody);

		}
	}
	
	
	
	public String[][] listDataJobs() throws  Exception {
		
		String[][] jobs = new String[0][2];
		OkHttpClient client = new OkHttpClient().newBuilder()
				  .build();
		Request request = new Request.Builder()
				  .url(this.celonis_cloud_url+this.base_url+"data-push/"+this.dataPoolId+"/jobs/")
				  .method("GET", null)
				  .addHeader("Content-Type", "application/json")
				  .addHeader("Authorization", "Bearer "+this.bearer_token)
				  .build();
		Response response = client.newCall(request).execute();
		if (response.isSuccessful()) {
			JSONArray responseObj = new JSONArray(response.body().string());
			jobs = new String[responseObj.length()][2];
			int index = 0;
			for (Iterator iterator = responseObj.iterator(); iterator.hasNext();) {
				JSONObject jobObj = (JSONObject) iterator.next();
				jobs[index][0]  = jobObj.getString("id");
				jobs[index][1]  = jobObj.getString("status");
				index++;
			}
			return jobs;
			
			
		}
		else {
			String responsebody = response.body().string();
			throw new Exception("ERROR "+response.code()+":"+responsebody);

		}
	}
	
	
	public String deleteJob(String jobId) throws  Exception {
		OkHttpClient client = new OkHttpClient().newBuilder()
				  .build();
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, "");
		Request request = new Request.Builder()
				  .url(this.celonis_cloud_url+this.base_url+"data-push/"+this.dataPoolId+"/jobs/"+jobId)
				  .method("DELETE", body)
				  .addHeader("Content-Type", "application/json")
				  .addHeader("Authorization", "Bearer "+this.bearer_token)
				  .build();
		Response response = client.newCall(request).execute();
		
		if (response.isSuccessful()) {
			return "SUCCESS";
			
		}
		else {
			String responsebody = response.body().string();
			throw new Exception("ERROR "+response.code()+":"+responsebody);

		}
	}
	
	
	
	public String[] getDatasModelTables(String dataModelId) throws  Exception {
		
		String[] tables = new String[0];
		
		OkHttpClient client = new OkHttpClient().newBuilder()
				  .build();
		Request request = new Request.Builder()
				  .url(this.celonis_cloud_url+this.base_url+"data-pools/"+this.dataPoolId+"/data-models/"+dataModelId+"/tables")
				  .method("GET", null)
				  .addHeader("Content-Type", "application/json")
				  .addHeader("Authorization", "Bearer "+this.bearer_token)
				  .build();
		Response response = client.newCall(request).execute();
		
		if (response.isSuccessful()) {
			JSONArray responseObj = new JSONArray(response.body().string());
			tables = new String[responseObj.length()];
			int index = 0;
			for (Iterator iterator = responseObj.iterator(); iterator.hasNext();) {
				JSONObject tableObj = (JSONObject) iterator.next();
				tables[index]  = tableObj.getString("id");
				index++;
			}
			return tables;

		}
		else {
			String responsebody = response.body().string();
			throw new Exception("ERROR "+response.code()+":"+responsebody);

		}
	}
	
	
	
	public String dataModelLoad(String dataModelId, String[] tables) throws  Exception {
		OkHttpClient client = new OkHttpClient().newBuilder()
				  .build();
		MediaType mediaType = MediaType.parse("application/json");
		JSONArray tableObj= new JSONArray(tables);
		RequestBody body = RequestBody.create(mediaType, tableObj.toString());
		Request request = new Request.Builder()
				  .url(this.celonis_cloud_url+this.base_url+"data-pools/"+this.dataPoolId+"/data-models/"+dataModelId+"/load")
				  .method("POST", body)
				  .addHeader("Content-Type", "application/json")
				  .addHeader("Authorization", "Bearer "+this.bearer_token)
				  .build();
		Response response = client.newCall(request).execute();
		if (response.isSuccessful()) {
			return "SUCCESS";
			
		}
		else {
			String responsebody = response.body().string();
			throw new Exception("ERROR "+response.code()+":"+responsebody);

		}
	}
	
	
	private boolean isJSONValid(String test) {
	    try {
	        new JSONObject(test);
	    } catch (JSONException ex) {
	        try {
	            new JSONArray(test);
	        } catch (JSONException ex1) {
	            return false;
	        }
	    }
	    return true;
	}

	public String newGenericDataPushJob(String type, String targetName, Map<String,String> csvParsingOptions, String strategy,String[][] table) throws  Exception {
		OkHttpClient client = new OkHttpClient().newBuilder().build();
		MediaType mediaType = MediaType.parse("application/json");
		JSONObject bodyJson = new JSONObject();
		bodyJson.put("type",type);
		bodyJson.put("fileType","CSV");
		bodyJson.put("dataPoolId",this.dataPoolId);
		bodyJson.put("targetName",targetName);
		bodyJson.put("upsertStrategy",strategy);
		
		JSONObject parsingOptions = new JSONObject();
		for (Entry<String, String> entry : csvParsingOptions.entrySet()) {
			parsingOptions.put(entry.getKey(),entry.getValue());
			
		}
		bodyJson.put("csvParsingOptions", parsingOptions);
		
		if (table.length > 0) {
			JSONObject tableSchema = new JSONObject();
			tableSchema.put("tableName", targetName);
			JSONArray tableColumns = new JSONArray();
			for (int i = 0; i < table.length; i++) {
				JSONObject columnObject = new JSONObject("{\"columnName\":\""+table[i][0]+"\",\"columnType\":\""+table[i][1]+"\",\"fieldLength\":"+table[i][2]+"}");
				tableColumns.put(columnObject);
			}
			tableSchema.put("columns",tableColumns);
		
			bodyJson.put("tableSchema",tableSchema);
		
		}
		RequestBody body = RequestBody.create(mediaType, bodyJson.toString());
		Request request = new Request.Builder()
			  .url(this.celonis_cloud_url+this.base_url+"data-push/"+this.dataPoolId+"/jobs/")
			  .method("POST", body)
			  .addHeader("Content-Type", "application/json")
			  .addHeader("Authorization", "Bearer "+this.bearer_token)
			  .build();
		Response response = client.newCall(request).execute();
		
		if (response.isSuccessful()) {
			JSONObject responseObj = new JSONObject(response.body().string());
			return responseObj.getString("id");
			
		}
		else {
			String responsebody = response.body().string();
			throw new Exception("ERROR "+response.code()+":"+responsebody);
	
		}
	}

}
