package com.automationanywhere.botcommand.celonis.utils;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;

import org.json.JSONObject;


import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;





public class CROperations {

	
	private static final Logger logger = LogManager.getLogger(CROperations.class);

    public static final Integer maxEvents = 1000;

    private String crurl ;
	private String token ;

	
	
	public CROperations setToken(String tokenValue) {
		this.token = tokenValue;
		return this;
	}


	public CROperations setCrurl(String  url) {
		this.crurl = url;
		return this;
		
	}
	
	

	
	
	public static boolean isInteger(String str)
    {
        for (char c : str.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

	
	
	
   public Integer getAuditEvents(String operator, String operands[][],String filter, String filename, Integer offset,Boolean append) throws Exception {
	   	
	   OkHttpClient client = new OkHttpClient().newBuilder()
	   											.readTimeout(30, TimeUnit.SECONDS)
	   											.build();
		JSONObject bodyObj = new JSONObject();
		
		
		JSONObject sortObj = new JSONObject("{\"field\":\"createdOn\", \"direction\": \"desc\"}");
		JSONArray sortArrayObj = new JSONArray();
		sortArrayObj.put(sortObj);
		bodyObj.put("sort", sortArrayObj);
		if (operands.length > 0) {
			JSONObject filterObj = new JSONObject();
			filterObj.put("operator", operator);
			
			JSONArray operandsObj = new JSONArray();
			for (int i = 0; i < operands.length; i++) {
				JSONObject operandObj = new JSONObject();
				operandObj.put("field", operands[i][0]);
				operandObj.put("operator", operands[i][1]);
				operandObj.put("value", operands[i][2]);
				operandsObj.put(operandObj);
			}
			
			filterObj.put("operands", operandsObj);
			
			bodyObj.put("filter", filterObj);
		}
		
		if (filter != "") {
			filter = filter.replaceAll("\n", "").replaceAll("\r", "").replaceAll("\t", "");
			JSONObject filterObj = new JSONObject(filter);
			bodyObj.put("filter", filterObj);
		}
		
		
		JSONObject pageObj = new JSONObject("{\"length\":\""+maxEvents+"\",\"offset\":\""+offset.toString()+"\"}");
		bodyObj.put("page", pageObj);
		
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, bodyObj.toString());
		Request request = new Request.Builder()
				.url(this.crurl+"/v1/audit/messages/list")
				.method("POST", body)
				.addHeader("Content-Type", "application/json")
				.addHeader("X-Authorization", this.token)
				.build();
		Response response = client.newCall(request).execute();
		
		
		if (response.isSuccessful()) {
			JSONObject responseObj = new JSONObject(response.body().string());
			JSONArray eventListObj = responseObj.getJSONArray("list");

			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename,append), StandardCharsets.UTF_8));
			if (!append) {
				String header = "\"Status\",\"Time\",\"Item name\",\"Event type\",\"Event started by\",\"Source device\",\"Source\",\"Request ID\"\n";
				writer.append(header);
			}
			for (Iterator iterator = eventListObj.iterator(); iterator.hasNext();) {
				JSONObject event = (JSONObject) iterator.next();
				String timestamp = event.getString("createdOn").replace("T", " ").replace("Z",""); 
				String eventEntry = "\""+event.getString("status")+"\",\""+timestamp+"\",\""+event.getString("objectName")+"\",\""+event.getString("activityType")+"\",\""+event.getString("userName")+"\",\""+event.getString("hostName")+"\",\""+event.getString("source")+"\",\""+event.getString("requestId")+"\"\n";
				writer.append(eventEntry);
			}
		    writer.close();
	     	return 	eventListObj.length();
			
		}
		else {
			JSONObject errorObj = new JSONObject(response.body().string());
			throw new Exception("ERROR "+response.code()+":"+errorObj.getString("message"));
		}

   }


public Integer getAARIEvents( String filename,List<String> processnames, Integer offset,Boolean append) throws Exception {
		
		int rows =0;
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename,append), StandardCharsets.UTF_8));
		if (!append) {
			String header = "\"Request ID\",\"Request Title\",\"Time\",\"Process Name\",\"Task ID\",\"Task Title\",\"Task Type\",\"User\",\"Status\"\n";
			writer.append(header);
		}
		OkHttpClient client = new OkHttpClient().newBuilder()
								.readTimeout(30, TimeUnit.SECONDS)
								.build();
		JSONObject bodyObj = new JSONObject();
		
		JSONObject sortObj = new JSONObject("{\"field\":\"createdOn\", \"direction\": \"desc\"}");
		JSONArray sortArrayObj = new JSONArray();
		sortArrayObj.put(sortObj);
		bodyObj.put("sort", sortArrayObj);
		JSONObject pageObj = new JSONObject("{\"length\":\""+maxEvents+"\",\"offset\":\""+offset.toString()+"\"}");
		bodyObj.put("page", pageObj);
		
		bodyObj.put("sort", sortArrayObj);
		if (processnames != null) {
			JSONObject filterObj = new JSONObject();
			filterObj.put("operator", "and");
			JSONArray parentoperandsObj = new JSONArray();
			JSONObject parentOperatorObj = new JSONObject();
			parentOperatorObj.put("operator", "or");
			JSONArray operandsObj = new JSONArray();
			for (Iterator iterator = processnames.iterator(); iterator.hasNext();) {
				String processname = (String) iterator.next();
				JSONObject operandObj = new JSONObject();
				operandObj.put("field", "process");
				operandObj.put("operator", "eq");
				operandObj.put("value", processname);
				operandsObj.put(operandObj);
			}
			parentOperatorObj.put("operands", operandsObj);
			parentoperandsObj.put(parentOperatorObj);
			filterObj.put("operands", parentoperandsObj);
			
			bodyObj.put("filter", filterObj);
		}

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, bodyObj.toString());
		Request request = new Request.Builder()
				  .url(this.crurl+"/aari/v2/requests/list")
				  .method("POST", body)
				  .addHeader("X-Authorization", this.token)
				  .addHeader("Content-Type", "application/json")
				  .build();
		Response response = client.newCall(request).execute();
		
		if (response.isSuccessful()) {
			JSONObject responseObj = new JSONObject(response.body().string());
			JSONArray requestListObj = responseObj.getJSONArray("list");
			rows = requestListObj.length();
			
			for (Iterator iterator = requestListObj.iterator(); iterator.hasNext();) {
				JSONObject requestObj = (JSONObject) iterator.next();
				
				String processname = requestObj.getString("process");
				String requesttitle = requestObj.getString("title");
				String requestid = String.valueOf(requestObj.getInt("id"));
				
				request = new Request.Builder()
						  .url(this.crurl+"/aari/v2/requests/"+requestid+"?showHidden=true")
						  .method("GET", null)
						  .addHeader("X-Authorization", this.token)
						  .addHeader("Content-Type", "application/json")
						  .build();
				response = client.newCall(request).execute();
				if (response.isSuccessful()) {
					responseObj = (JSONObject) new JSONObject(response.body().string());
					JSONArray stepsListObj = responseObj.getJSONArray("steps");
					for (Iterator iterator2 = stepsListObj.iterator(); iterator2.hasNext();) {
						JSONObject step = (JSONObject) iterator2.next();
						String steptype = step.getString("type");
						String stepname = step.getString("name");
						String stepid = String.valueOf(step.getInt("id"));
						String createdOn = step.getString("createdOn").replace("T", " ").replace("Z","").substring(0,19);
						String status = step.getString("status");
						String user = "bot";
						if (step.has("assignee")) {
							user = step.getJSONObject("assignee").getString("username");
						}
						
						String taskEntry = "\""+requestid+"\",\""+requesttitle+"\",\""+createdOn+"\",\""+processname+"\",\""+stepid+"\",\""+stepname+"\",\""+steptype+"\",\""+user+"\",\""+status+"\"\n";
						writer.append(taskEntry);
					}
				}
				else {
				    writer.close();
					JSONObject errorObj = new JSONObject(response.body().string());
					throw new Exception("ERROR "+response.code()+":"+errorObj.getString("message"));
				}
				
			}
		}
		else {
			writer.close();
			JSONObject errorObj = new JSONObject(response.body().string());
			throw new Exception("ERROR "+response.code()+":"+errorObj.getString("message"));
		}
			
		writer.close();
		return rows;
		
   }



}
