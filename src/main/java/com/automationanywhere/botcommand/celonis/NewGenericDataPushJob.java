/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.celonis;

import com.automationanywhere.botcommand.celonis.utils.CelonisEventCollection;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.data.model.table.Row;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import static com.automationanywhere.commandsdk.model.AttributeType.SELECT;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "newGenericDataPushJob", label = "New Generic Push Job", 
		node_label = "New Generic Job {{targetname}}", description = "New Generic Push Job", comment = true ,  text_color = "#000000" , background_color =  "#c6cacc" , icon = "pkg.svg", 
		return_label = "Job Id", return_type = DataType.STRING, return_required = true)

public class NewGenericDataPushJob {
	
    @Sessions
    private Map<String, Object> sessions;
  

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Celonis") @NotEmpty String sessionName,
							  @Idx(index = "2", type = SELECT, options = {
									  @Idx.Option(index = "2.1", pkg = @Pkg(label = "Delta", value = "DELTA")),
									  @Idx.Option(index = "2.2", pkg = @Pkg(label = "Replace", value = "REPLACE"))
							      }) @Pkg(label = "Type", default_value = "DELTA", default_value_type = STRING) @NotEmpty String type,
							  @Idx(index = "3", type = SELECT, options = {
									  @Idx.Option(index = "3.1", pkg = @Pkg(label = "Unchanged metadata", value = "UPSERT_WITH_UNCHANGED_METADATA")),
									  @Idx.Option(index = "3.2", pkg = @Pkg(label = "With nullification", value = "UPSERT_WITH_NULLIFICATION"))
							      }) @Pkg(label = "Upload Strategy", default_value = "UPSERT_WITH_UNCHANGED_METADATA", default_value_type = STRING) @NotEmpty String strategy,
						      @Idx(index = "4", type = TEXT)  @Pkg(label = "Target Name" , default_value_type = STRING ) @NotEmpty String targetname,
						      @Idx(index = "5", type = AttributeType.TABLE)  @Pkg(label = "Table Columns" , description = "Column Name,Column Type (INTEGER,DATE,TIME,DATETIME,FLOAT,BOOLEAN,STRING),Column Field Length",  default_value_type = DataType.TABLE ) @NotEmpty Table tableColumns,
						      @Idx(index = "6", type = AttributeType.DICTIONARY)  @Pkg(label = "CSV Parsing Options" , description = "Options: charSet,dateFormat,decimalSeparator,escapeSequence,lineEnding,quoteSequence,separatorSequence,thousandSeparator",  default_value_type = DataType.DICTIONARY ) Map<String,Value> parsingOptions

						      ) throws Exception	
   {


		CelonisEventCollection celonisCollection  = (CelonisEventCollection) this.sessions.get(sessionName);  

		Map<String, String> parsingOptionMap = new HashMap<String,String>();;
		for (Entry<String, Value> entry : parsingOptions.entrySet()) {
			parsingOptionMap.put(entry.getKey(), entry.getValue().toString());
		}
		
    	String[][] table = new String[0][3];
    	if (tableColumns != null) {
    		List<Row> rows = tableColumns.getRows();
    		table = new String[rows.size()][3];
    		int i =0;
    		for (Iterator iterator = rows.iterator(); iterator.hasNext();) {
				Row row = (Row) iterator.next();
				List<Value> values = row.getValues();
				table[i][0] = values.get(0).toString();
				table[i][1] = values.get(1).toString();
				table[i][2] = values.get(2).toString();
				i++;
			}
				
    	}
		
		String jobid = celonisCollection.newGenericDataPushJob(type, targetname, parsingOptionMap, strategy, table);

		return new StringValue(jobid);

    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
   }
}
