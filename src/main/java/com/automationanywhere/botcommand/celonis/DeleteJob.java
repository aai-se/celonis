/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.celonis;

import com.automationanywhere.botcommand.celonis.utils.CelonisEventCollection;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;

import com.automationanywhere.commandsdk.model.DataType;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;


import java.util.Map;


/** 
 * @author Stefan Karsten
 */
@BotCommand
@CommandPkg(
		//Unique name inside a package and label to display.
		name = "deletejob", label = "Delete Job", 
		node_label = "Delete Job {{jobid}}", description = "Delete Data Push Job", comment = true ,  text_color = "#000000" , background_color =  "#c6cacc" , icon = "pkg.svg", 
		return_label = "Status", return_type = DataType.STRING, return_required = false)

public class DeleteJob {
	
    @Sessions
    private Map<String, Object> sessions;
  

	//Identify the entry point for the action. Returns a Value<String> because the return type is String. 
	@Execute
	public StringValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Celonis") @NotEmpty String sessionName,
						      @Idx(index = "2", type = TEXT)  @Pkg(label = "Job Id" , default_value_type = STRING ) @NotEmpty String jobid
						      ) throws Exception	
   {


		CelonisEventCollection celonisCollection  = (CelonisEventCollection) this.sessions.get(sessionName);  

		String status = celonisCollection.deleteJob(jobid);

		return new StringValue(status);

    }


	public void setSessions(Map<String, Object> sessions) {
		this.sessions = sessions;
   }
}
