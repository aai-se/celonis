/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */

package com.automationanywhere.botcommand.celonis;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

import java.util.Map;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.celonis.utils.CelonisEventCollection;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;
import com.automationanywhere.commandsdk.annotations.Execute;
import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.core.security.SecureString;





/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Start Celonis session", name = "StartCelonisSession",comment = true ,  text_color = "#000000" , background_color =  "#c6cacc" ,  description = "Start new Celonis session", 
icon = "pkg.svg" , node_label = "Start Session {{sessionName}}|") 
public class StartSession {
 
    @Sessions
    private Map<String, Object> sessions;
    
	  
	@com.automationanywhere.commandsdk.annotations.GlobalSessionContext
	private GlobalSessionContext globalSessionContext;

	  
	  public void setGlobalSessionContext(GlobalSessionContext globalSessionContext) {
	        this.globalSessionContext = globalSessionContext;
	    }
	  
	  
   private CelonisEventCollection celonisCollection;
	
    
    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session name",  default_value_type = STRING, default_value = "Celonis") @NotEmpty String sessionName,
    		          @Idx(index = "2", type = TEXT) @Pkg(label = "Celonis Base URL",  default_value_type = STRING) @NotEmpty String url,
	 				  @Idx(index = "3", type = AttributeType.CREDENTIAL) @Pkg(label = "Bearer Token",  default_value_type = STRING) @NotEmpty SecureString token,
	 				  @Idx(index = "4", type = AttributeType.CREDENTIAL) @Pkg(label = "Data Pool ID",  default_value_type = STRING) @NotEmpty SecureString datapoolid
    		) throws Exception {
 
        
        
    /*    String token = this.globalSessionContext.getUserToken();
        String url = this.globalSessionContext.getCrUrl();
        String executionId = this.globalSessionContext.getExecutionId();
       
 */
        this.celonisCollection = new CelonisEventCollection(url, token.getInsecureString(), datapoolid.getInsecureString());
        this.sessions.put(sessionName, this.celonisCollection);


    }
 
    
    
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
    

    
    
 
    
    
}