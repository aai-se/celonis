package com.automationanywhere.botcommand.celonis;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class NewGenericDataPushJobCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(NewGenericDataPushJobCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    NewGenericDataPushJob command = new NewGenericDataPushJob();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(convertedParameters.get("sessionName") !=null && !(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("type") && parameters.get("type") != null && parameters.get("type").get() != null) {
      convertedParameters.put("type", parameters.get("type").get());
      if(convertedParameters.get("type") !=null && !(convertedParameters.get("type") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","type", "String", parameters.get("type").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("type") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","type"));
    }
    if(convertedParameters.get("type") != null) {
      switch((String)convertedParameters.get("type")) {
        case "DELTA" : {

        } break;
        case "REPLACE" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","type"));
      }
    }

    if(parameters.containsKey("strategy") && parameters.get("strategy") != null && parameters.get("strategy").get() != null) {
      convertedParameters.put("strategy", parameters.get("strategy").get());
      if(convertedParameters.get("strategy") !=null && !(convertedParameters.get("strategy") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","strategy", "String", parameters.get("strategy").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("strategy") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","strategy"));
    }
    if(convertedParameters.get("strategy") != null) {
      switch((String)convertedParameters.get("strategy")) {
        case "UPSERT_WITH_UNCHANGED_METADATA" : {

        } break;
        case "UPSERT_WITH_NULLIFICATION" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","strategy"));
      }
    }

    if(parameters.containsKey("targetname") && parameters.get("targetname") != null && parameters.get("targetname").get() != null) {
      convertedParameters.put("targetname", parameters.get("targetname").get());
      if(convertedParameters.get("targetname") !=null && !(convertedParameters.get("targetname") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","targetname", "String", parameters.get("targetname").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("targetname") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","targetname"));
    }

    if(parameters.containsKey("tableColumns") && parameters.get("tableColumns") != null && parameters.get("tableColumns").get() != null) {
      convertedParameters.put("tableColumns", parameters.get("tableColumns").get());
      if(convertedParameters.get("tableColumns") !=null && !(convertedParameters.get("tableColumns") instanceof Table)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","tableColumns", "Table", parameters.get("tableColumns").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("tableColumns") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","tableColumns"));
    }

    if(parameters.containsKey("parsingOptions") && parameters.get("parsingOptions") != null && parameters.get("parsingOptions").get() != null) {
      convertedParameters.put("parsingOptions", parameters.get("parsingOptions").get());
      if(convertedParameters.get("parsingOptions") !=null && !(convertedParameters.get("parsingOptions") instanceof Map)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","parsingOptions", "Map", parameters.get("parsingOptions").get().getClass().getSimpleName()));
      }
    }

    command.setSessions(sessionMap);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("type"),(String)convertedParameters.get("strategy"),(String)convertedParameters.get("targetname"),(Table)convertedParameters.get("tableColumns"),(Map<String, Value>)convertedParameters.get("parsingOptions")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
