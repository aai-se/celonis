package com.automationanywhere.botcommand.celonis;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.model.table.Table;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.Boolean;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Number;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class SaveAuditEventsCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(SaveAuditEventsCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    SaveAuditEvents command = new SaveAuditEvents();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("sessionName") && parameters.get("sessionName") != null && parameters.get("sessionName").get() != null) {
      convertedParameters.put("sessionName", parameters.get("sessionName").get());
      if(convertedParameters.get("sessionName") !=null && !(convertedParameters.get("sessionName") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","sessionName", "String", parameters.get("sessionName").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("sessionName") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","sessionName"));
    }

    if(parameters.containsKey("filename") && parameters.get("filename") != null && parameters.get("filename").get() != null) {
      convertedParameters.put("filename", parameters.get("filename").get());
      if(convertedParameters.get("filename") !=null && !(convertedParameters.get("filename") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","filename", "String", parameters.get("filename").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("filename") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","filename"));
    }

    if(parameters.containsKey("offset") && parameters.get("offset") != null && parameters.get("offset").get() != null) {
      convertedParameters.put("offset", parameters.get("offset").get());
      if(convertedParameters.get("offset") !=null && !(convertedParameters.get("offset") instanceof Number)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","offset", "Number", parameters.get("offset").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("offset") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","offset"));
    }

    if(parameters.containsKey("append") && parameters.get("append") != null && parameters.get("append").get() != null) {
      convertedParameters.put("append", parameters.get("append").get());
      if(convertedParameters.get("append") !=null && !(convertedParameters.get("append") instanceof Boolean)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","append", "Boolean", parameters.get("append").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("append") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","append"));
    }

    if(parameters.containsKey("filtertype") && parameters.get("filtertype") != null && parameters.get("filtertype").get() != null) {
      convertedParameters.put("filtertype", parameters.get("filtertype").get());
      if(convertedParameters.get("filtertype") !=null && !(convertedParameters.get("filtertype") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","filtertype", "String", parameters.get("filtertype").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("filtertype") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","filtertype"));
    }
    if(convertedParameters.get("filtertype") != null) {
      switch((String)convertedParameters.get("filtertype")) {
        case "TABLE" : {
          if(parameters.containsKey("operand") && parameters.get("operand") != null && parameters.get("operand").get() != null) {
            convertedParameters.put("operand", parameters.get("operand").get());
            if(convertedParameters.get("operand") !=null && !(convertedParameters.get("operand") instanceof String)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","operand", "String", parameters.get("operand").get().getClass().getSimpleName()));
            }
          }
          if(convertedParameters.get("operand") == null) {
            throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","operand"));
          }
          if(convertedParameters.get("operand") != null) {
            switch((String)convertedParameters.get("operand")) {
              case "and" : {

              } break;
              case "or" : {

              } break;
              default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","operand"));
            }
          }

          if(parameters.containsKey("operands") && parameters.get("operands") != null && parameters.get("operands").get() != null) {
            convertedParameters.put("operands", parameters.get("operands").get());
            if(convertedParameters.get("operands") !=null && !(convertedParameters.get("operands") instanceof Table)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","operands", "Table", parameters.get("operands").get().getClass().getSimpleName()));
            }
          }


        } break;
        case "JSON" : {
          if(parameters.containsKey("filter") && parameters.get("filter") != null && parameters.get("filter").get() != null) {
            convertedParameters.put("filter", parameters.get("filter").get());
            if(convertedParameters.get("filter") !=null && !(convertedParameters.get("filter") instanceof String)) {
              throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","filter", "String", parameters.get("filter").get().getClass().getSimpleName()));
            }
          }


        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","filtertype"));
      }
    }

    command.setSessions(sessionMap);
    command.setGlobalSessionContext(globalSessionContext);
    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("sessionName"),(String)convertedParameters.get("filename"),(Number)convertedParameters.get("offset"),(Boolean)convertedParameters.get("append"),(String)convertedParameters.get("filtertype"),(String)convertedParameters.get("operand"),(Table)convertedParameters.get("operands"),(String)convertedParameters.get("filter")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }

  public Map<String, Value> executeAndReturnMany(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return null;
  }
}
